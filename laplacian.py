import numpy as np
from sklearn import neighbors
from scipy import sparse

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline


def laplacianSG(data, t, sigma):
        n = data.shape[0]
        #compute nieghborhood structure that includes all neighbors 
        #within distance 3*t ## changed this to t_factor  
        #G = neighbors.radius_neighbors_graph( data, radius, mode="distance", include_self = True )
        #G = neighbors.radius_neighbors_graph( data, radius, mode="distance", include_self = True )
        #G = neighbors.kneighbors_graph( data, knn, mode="distance", include_self = True)
        G = neighbors.radius_neighbors_graph( data, sigma* np.sqrt(t), mode="distance", include_self = True )

        #Compute sparse graph Laplacian
        #currently not normalized by phi(x)
        L = G

        #weighted laplacian
        L.data = L.data**2 
        #adaptive kernel
        #t = sparse.spdiags( 1 /  np.power( 0.5 * np.transpose( L.sum( axis=1 ) ) / knn, 2 ), 0, n, n )

        #L = t.dot(L)
        L.data = L.data / (2*t)
        L.data =  np.exp( -L.data  ) 
        L.setdiag(1)

        phi = sparse.spdiags(  np.transpose( L.sum( axis=1 ) ), 0, n, n)  

        L = phi -  L

        phi.data  =  2 / ( phi.data * np.sqrt(t) )
        L = phi.dot( L )

       
        return L


def laplacianMW(data, t, sigma):
        n = data.shape[0]
        
        G = neighbors.radius_neighbors_graph( data, sigma* np.sqrt(t), mode="distance", include_self = True )

        print 'average # of knn' , len(G.data)/n
        #Compute sparse graph Laplacian
        #currently not normalized by phi(x)
        #L = G
        K = G
        I = sparse.identity
        K.data = K.data**2   #distance squared matrix

        K.data =  np.exp( -K.data /(2*t) )
        density = K.sum( axis=1 )
        print "sigma:", sigma
        print "n, t:", n, t
        print "average density:", density.mean()
        print "minimum nonzero kernel entry:", K.data.min()
        dd_err = density/(density -1)   #this is to correct fact that density includes point in addition to points nearby
        print "average density offset", dd_err.mean()
        K.setdiag(1)   #this should be unnormalized kernel
        s = sparse.spdiags(1/np.transpose( K.sum( axis=1 )), 0, n, n )
        L = sparse.identity(n)-s.dot(K)
        s = sparse.spdiags(np.transpose( dd_err), 0, n, n )
        L = (-2/t)*s.dot(L)

        return L


#use quadratic fit to compute laplacian
class FitLaplacian:

    def __init__(self, data, knn, d):
        G = neighbors.kneighbors_graph( data, knn, mode="distance", include_self = True)
        self.d = d
        self.Xp = []
        self.index = []
        for i in range( data.shape[0] ):
            #print "Computing point " + str(i)
            index = G.indices[ G.indptr[i]:G.indptr[i+1] ]   
            #dists = G.data[index]]
            X = data[index, :] - data[i, :]
            #print "  SVD on " + str(X.shape) + " points"
            U, s, V = np.linalg.svd(X)
            self.Xp.append( X.dot( np.transpose( V[range(d), :]) ) )
            self.index.append( index ) 

    def dot(self, f ):
        L = np.zeros( len( f ) )
        
        poly = PolynomialFeatures(degree=2)
        linear = LinearRegression()
        model = Pipeline( [ ("polynomial_features", poly),
                            ("linear_regression", linear) ] )
        for i in range( len( self.Xp ) ):
            model.fit( self.Xp[i], f[ self.index[i] ] )
            tmp = 0
            for k in range( poly.powers_.shape[0] ):
                if np.sum( poly.powers_[k, :] ) == 2:
                    #print str( poly.powers_[k, ] ) + " : " + str( linear.coef_[k] )
                    mixed = True
                    for p in poly.powers_[k, :] :
                        if p == 2:
                            mixed = False
                            break;

                    if mixed == False :
                        tmp += linear.coef_[k]

            L[i] = tmp
        
        return L

        

def main():
    pass

if __name__ == '__main__':
    main()

