
import laplacian
import ricci
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.sparse.linalg import eigs

import fibsphere

t = .05
n = 2500

#do full laplacian
sigma = 5


# Flats
## Random sampled
sample = np.zeros(shape=(n,2))
sample[:,0]=np.random.uniform(0,1, n)
sample[:,1]=np.random.uniform(0,2, n)

## Regular grid 
x = np.linspace(0, 1, round( np.sqrt(n) ) )
y = np.linspace(0, 2, round( np.sqrt(n) ) )
xv, yv = np.meshgrid(x, y)
xv = xv.flatten()
yv = yv.flatten()
grid = np.transpose( np.vstack( [ xv, yv] ) )


#Sphere


#Random sample
sphere = np.random.normal(size = (3, n) )
sphere = np.transpose(sphere / np.sqrt( (sphere**2).sum(axis=0) )  )
sample = sphere

#Fibonacci sphere

grid = fibsphere.fibonacci_sphere(n)

L1 = laplacian.laplacianSG(sample, t, sigma)
L2 = laplacian.laplacianMW(sample, t, sigma)

L3 = laplacian.FitLaplacian(sample, 25, 2)

L1g = laplacian.laplacianSG(grid, t, sigma)
L2g = laplacian.laplacianMW(grid, t, sigma)

L3g = laplacian.FitLaplacian(grid, 25, 2)

#swissroll, color = datasets.make_swiss_roll(n_samples=n)	
#data = swissroll
Lf1 = L1.dot(sample[:,0])
Lf2 = L2.dot(sample[:,0])
Lf3 = L3.dot(sample[:,0])

Lf1g = L1g.dot(grid[:,0])
Lf2g = L2g.dot(grid[:,0])
Lf3g = L3g.dot(grid[:,0])

fig, ((ax1, ax2, ax3), (ax4, ax5, ax6) ) = plt.subplots(2, 3, sharex='col')

ax1.scatter(sample[:,0], Lf1)
ax1.set_title('Laplacian SG random uniform')
ax2.scatter(sample[:,0], Lf2)
ax2.set_title('Laplacian MW random uniform')
ax3.scatter(sample[:,0], Lf3)
ax3.set_title('Laplacian fit random uniform')


ax4.scatter(grid[:,0], Lf1g)
ax4.set_title('Laplacian SG grid')
ax5.scatter(grid[:,0], Lf2g)
ax5.set_title('Laplacian MW grid')
ax6.scatter(grid[:,0], Lf3g)
ax6.set_title('Laplacian fit grid')

plt.show()

L1vals, L1vecs = eigs(L1, k=6, which='SM')
L2vals, L2vecs = eigs(L2, k=6,which='SM')
L1gvals, L1gvecs = eigs(L1g, k=6, which='SM')
L2gvals, L2gvecs = eigs(L2g, k=6, which='SM')

print L1vecs.shape

fig = plt.figure(figsize=plt.figaspect(2.))
ax1 = fig.add_subplot(2, 2, 1, projection='3d')
ax2 = fig.add_subplot(2, 2, 2, projection='3d')
ax3 = fig.add_subplot(2, 2, 3, projection='3d')
ax4 = fig.add_subplot(2, 2, 4, projection='3d')

ax1.scatter(sample[:,0], sample[:, 1], sample[:,2], c=L1vecs[:,1] )
ax1.set_title('Laplacian SG random uniform')
ax2.scatter(sample[:,0], sample[:, 1], sample[:,2], c=L2vecs[:,1] )
ax2.set_title('Laplacian MW random uniform')
ax3.scatter(grid[:,0], grid[:, 1], grid[:,2], c=L1gvecs[:,1] )
ax3.set_title('Laplacian SG grid')
ax4.scatter(grid[:,0], grid[:, 1], grid[:,2], c=L2gvecs[:,1] )
ax4.set_title('Laplacian MW grid')

plt.show()


print "\n L sg random: "
Gamma = ricci.ricci(sample, L1, 20)
print "\n L mw random: "
Gamma = ricci.ricci(sample, L2, 20)


print "\n L sg grid: "
Gamma = ricci.ricci(grid, L1g, 20)
print "\n L mw grid: "
Gamma = ricci.ricci(grid, L2g, 20)


print "\n L fit random: "
Gamma = ricci.ricci(sample, L3, 20)
print "\n L fit sample: "
Gamma = ricci.ricci(grid, L3g, 20)
