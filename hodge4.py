import numpy as np
from sklearn import neighbors
from scipy import sparse
from sklearn import datasets

from scipy.sparse.linalg import eigs

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

t = .001
d = 2
n = 15000
#radius = 0.1
knn = 10
sigma = 5

print 't, n, sigma:', t, n, sigma, 'make note of these so if it crashes so we do not try again'


##Sphere
sphere = np.random.normal(size = (3, n) )
sphere = np.transpose(sphere / np.sqrt( (sphere**2).sum(axis=0) )  )
data = sphere

## Flatsquare
flatsquare = np.zeros(shape=(n,3))
flatsquare[:,0]=np.random.rand(n)
flatsquare[:,1]=np.random.rand(n)
data = flatsquare


##Swiss Roll 
#swissroll, color = datasets.make_swiss_roll(n_samples=n)	
#data = swissroll

#compute neighborhood structure that includes all neighbors within distance 3*t ## changed this to t_factor  
#G = neighbors.radius_neighbors_graph( data, radius, mode="distance", include_self = True )
#G = neighbors.radius_neighbors_graph( data, radius, mode="distance", include_self = True )

try : G = neighbors.kneighbors_graph( data, knn, mode="distance", include_self = True)
except : 
	print 'more neighbors than points'
	pass

G = neighbors.radius_neighbors_graph( data, sigma* np.sqrt(t), mode="distance", include_self = True )

print 'average # of knn' , len(G.data)/n
#Compute sparse graph Laplacian
#currently not normalized by phi(x)
#L = G
K = G
I = sparse.identity
K.data = K.data**2   #distance squared matrix

K.data =  np.exp( -K.data /(2*t) )
density = K.sum( axis=1 )
print "sigma:", sigma
print "n, t:", n, t
print "average density:", density.mean()
print "minimum nonzero kernel entry:", K.data.min()
dd_err = density/(density -1)   #this is to correct fact that density includes point in addition to points nearby
print "average density offset", dd_err.mean()
K.setdiag(1)   #this should be unnormalized kernel
s = sparse.spdiags(1/np.transpose( K.sum( axis=1 )), 0, n, n )
L = sparse.identity(n)-s.dot(K)
s = sparse.spdiags(np.transpose( dd_err), 0, n, n )
L = (-2/t)*s.dot(L)

f = data[:,0]    ##Test on easy eigenvalue
Lf = L.dot(f)

fig = plt.figure()
plt.scatter(f, Lf, alpha=0.5)
plt.show()

quit()

#adaptive kernel

#Computed Gamma_2 for local pca functions

G = neighbors.kneighbors_graph( data, knn, mode="distance", include_self = True)
Gamma = []
for i in range(15):
  print "Computing point " + str(i)
  index = G.indices[ G.indptr[i]:G.indptr[i+1] ]   
  #dists = G.data[index]]
  X = data[index, :] - data[i, :]
  print "  SVD on " + str(X.shape) + " points"
  U, s, V = np.linalg.svd(X)
  f = sphere.dot( V[0, :] )
  h = sphere.dot( V[1, :] )
  print V.shape
  print sphere[i,:].dot(V[0, :]), "sphere[i,:].dot(V[0, :])"   # this is small, good.  
  print sphere[i,:].dot(V[1, :]), "sphere[i,:].dot(V[1, :])"   # this is small, good.  

  L_f = L.dot(f)
  L_h = L.dot(h)
  L_fh = L.dot(f*h)
  L_ff = L.dot(f*f)
  L_hh = L.dot(h*h)

  G1_f_h = 0.5 * ( L_fh - L_f * h - f * L_h )
  G1_f_f = 0.5 * ( L_ff - 2 * L_f * f )
  G1_h_h = 0.5 * ( L_hh - 2 * L_h * h )

  L_Lf = L.dot( L_f )
  L_Lf_f = L.dot( L_f * f)
  L_Lf_h = L.dot( L_f * h)

  L_Lh = L.dot( L_h )
  L_Lh_h = L.dot( L_h * h )
  L_Lh_f = L.dot( L_h * f )

  G1_Lf_h = 0.5 * ( L_Lf_h  -  L_Lf * h    -  L_f * L_h  )
  G1_Lf_f = 0.5 * ( L_Lf_f  -  L_Lf * f    -  L_f * L_f  )
  G1_Lh_f = 0.5 * ( L_Lh_f  -  L_f  * L_h  -    f * L_Lh )
  G1_Lh_h = 0.5 * ( L_Lh_h  -  L_Lh * h    -  L_h * L_h  )


  L_G1_f_h = L.dot( G1_f_h )
  L_G1_f_f = L.dot( G1_f_f )
  L_G1_h_h = L.dot( G1_h_h )


  G2_f_h = 0.5 * ( L_G1_f_h - G1_Lf_h - G1_Lh_f) 
  G2_f_f = 0.5 * ( L_G1_f_f - 2 * G1_Lf_f ) 
  G2_h_h = 0.5 * ( L_G1_h_h - 2 * G1_Lh_h ) 
  Gamma.append( [G2_f_f, G2_h_h, G2_f_h] )
  matrix = np.array([[G2_f_f[i],G2_f_h[i] ], [G2_f_h[i],G2_h_h[i] ]])
  print matrix
  print data[i,:]


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(data[:,0], data[:,1], data[:,2],  c=Gamma[0][1], alpha=0.1, s=20, cmap = "coolwarm")
plt.show()
