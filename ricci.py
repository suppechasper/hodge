import numpy as np
from sklearn import neighbors
from scipy import sparse



#use graph laplacian 
def ricci(data, L, knn):
        #Computed Gamma_2 for local pca functions
        Gamma = []
        G = neighbors.kneighbors_graph( data, knn, mode="distance", include_self = True)
        for i in range(5):
            print "Computing point " + str(i)
            index = G.indices[ G.indptr[i]:G.indptr[i+1] ]   
            #dists = G.data[index]]
            X = data[index, :] - data[i, :]
            #print "  SVD on " + str(X.shape) + " points"
            U, s, V = np.linalg.svd(X)
            f = data.dot( V[0, :] )
            h = data.dot( V[1, :] )
            #print V.shape
            #print data[i,:].dot(V[0, :]), "sphere[i,:].dot(V[0, :])"   # this is small, good.  

            L_f = L.dot(f)
            L_h = L.dot(h)
            L_fh = L.dot(f*h)
            L_ff = L.dot(f*f)
            L_hh = L.dot(h*h)

            G1_f_h = 0.5 * ( L_fh - L_f * h - f * L_h )
            G1_f_f = 0.5 * ( L_ff - 2 * L_f * f )
            G1_h_h = 0.5 * ( L_hh - 2 * L_h * h )

            L_Lf = L.dot( L_f )
            L_Lf_f = L.dot( L_f * f)
            L_Lf_h = L.dot( L_f * h)

            L_Lh = L.dot( L_h )
            L_Lh_h = L.dot( L_h * h )
            L_Lh_f = L.dot( L_h * f )

            G1_Lf_h = 0.5 * ( L_Lf_h  -  L_Lf * h    -  L_f * L_h  )
            G1_Lf_f = 0.5 * ( L_Lf_f  -  L_Lf * f    -  L_f * L_f  )
            G1_Lh_f = 0.5 * ( L_Lh_f  -  L_f  * L_h  -    f * L_Lh )
            G1_Lh_h = 0.5 * ( L_Lh_h  -  L_Lh * h    -  L_h * L_h  )


            L_G1_f_h = L.dot( G1_f_h )
            L_G1_f_f = L.dot( G1_f_f )
            L_G1_h_h = L.dot( G1_h_h )


            G2_f_h = 0.5 * ( L_G1_f_h - G1_Lf_h - G1_Lh_f) 
            G2_f_f = 0.5 * ( L_G1_f_f - 2 * G1_Lf_f ) 
            G2_h_h = 0.5 * ( L_G1_h_h - 2 * G1_Lh_h ) 
            Gamma.append( [G2_f_f, G2_h_h, G2_f_h] )
            print G2_f_f[i], G2_h_h[i], G2_f_h[i]

        return Gamma






def main():
    pass

if __name__ == '__main__':
    main()

