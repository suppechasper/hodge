
import laplacian
import ricci
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

t = .001
n = 15000
sigma = 5

#sphere = np.random.normal(size = (3, n) )
#sphere = np.transpose(sphere / np.sqrt( (sphere**2).sum(axis=0) )  )
#data = sphere

## Flatsquare
flatsquare = np.zeros(shape=(n,2))
flatsquare[:,0]=np.random.uniform(0,1, n)
flatsquare[:,1]=np.random.uniform(0,1, n)
data = flatsquare


L1 = laplacian.laplacianSG(data, t, sigma)
L2 = laplacian.laplacianMW(data, t, sigma)


#swissroll, color = datasets.make_swiss_roll(n_samples=n)	
#data = swissroll
f = data[:,0]
Lf1 = L1.dot(f)
Lf2 = L2.dot(f)

fig, ((ax1, ax2) ) = plt.subplots(2, 1, sharex='col', sharey='row')
ax1.scatter(f, Lf1)
ax1.set_title('Laplacian SG')
ax2.scatter(f, Lf2)
ax2.set_title('Laplacian MW')

plt.show()


quit()

Gamma = ricci.ricci(data, L1, 20)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(data[:,0], data[:,1], data[:,2],  c=Gamma[0][1], alpha=0.1, s=20, cmap = "coolwarm")
plt.show()
